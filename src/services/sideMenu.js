define(function(require){
    let gg = [
        {
            name : 'DASHBOARD',
            actived : true,
            subMenu : []
        },
        {
            name : 'POST',
            actived : true,
            subMenu : [
                {
                    name : 'NEW POST',
                    actived : true,
                }
            ]
        },
        {
            name : 'KATEGORI',
            actived : true,
            subMenu : [
                {
                    name : 'NEW POST',
                    actived : true,
                },
            ]
        },
        {
            name : 'TAG',
            actived : true,
            subMenu : [
                {
                    name : 'NEW TAG',
                    actived : true
                }
            ]
        },
        {
            name : 'PAGE',
            actived : true,
            subMenu : [
                {
                    name : 'NEW PAGE',
                    actived : true
                }
            ]
        }
    ];
    return gg;
})