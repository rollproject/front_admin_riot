define(function (require) {
    var riot = require('riot');
    var template = require('./sideMenu.html!');
    var jj = riot.tag('side-menu', template, function () {
        this.menuName = '';
        this.open = (whatMenu,e) => {
            if(this.menuName != whatMenu){
                this.menuName = whatMenu;
                this.isNextOpened = true;
            }else{
                if(this.isNextOpened){
                    this.isNextOpened = false;
                }else{
                    this.isNextOpened = true;
                }
            }
            this.update();
        }
        $(document).mouseup((e)=>{
            var container = $(".list_menu");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                this.isNextOpened = false;
                this.update();
            }else{
                
            }
        });
    });
    return jj;
});