define([
    'riot', 'require'
], function (riot, require) {
    var listTemplate = require('./list.html!');
    var navBarTemplate = require('./navBar.html!');
    var newTemplate = require('./new.html!');
    var listController = require('./listController');
    riot.tag('nav-bar', navBarTemplate, function () {})
    riot.tag('menu-list', listTemplate, listController);
    riot.tag('menu-new', newTemplate, function () {})
})

