define(function (require) {
    require('jquery');
    require('bootstrap-material-design/dist/js/material.min.js');
    require('bootstrap/js/dropdown.js');
    require('./components/basic/sideMenu.js');
    $.material.init();
    var riot = require('riot');
    var route = require('riot-route');
    var mainTemplate = require('/src/main.html!');
    var kk = riot.tag('main', mainTemplate, function () {
        this.sideMenuService = require('./services/sideMenu.js');
        console.log('24234234',this.sideMenuService);
        this.title = this.opts.tt;
        this.isCloseSideMenu = true;
        
        $('.wrap_dashboard').on('click',(e)=>{
            this.isCloseSideMenu = false;
            console.log('23333333');
        })
    });
    riot.mount(kk);
    route.stop();
    route.start();
    route.base('#/main/');
    var routes = {
        dashboard: function (action, id) {
            console.log(action, id);
        },
        'menu-manager': function (action, id) {
            switch (action) {
                case "list":
                    require('./modules/menuManager/index.js')
                    riot.mount('#content', 'menu-list', {title: 'Menu Manager List'});
                    console.log('aaaaaaaaaaaa');
                    break;
                case "new":
                    break;
                case "update":
                    break;
            }
        },
        wrong: function () {
            require('./modules/others/index.js')
            riot.mount('#content', 'wrong', {tt: 'aaaaaaaaaaaaaaaaaaaaaa'});
        },
        posts: function(action,id){
            switch(action){
                case "list":
                break;
                case "new":
                    require('./modules/posts/newPost.js');
                    riot.mount('#content','new-post',{});
                break;
                case "update":
                break;
                case "view":
                break;
            }
        }
    }
    var handler = function (collection, id, action) {
        // try {
            var routeFn = routes[collection];
            return routeFn(id, action);
        // } catch (ex) {
            // console.log('e234234', ex);
            // route('/wrong');
        // }

    }
    route(handler);
    // exec itu lansung execute current pathmu yang sekarang kamu refresh
    route.exec();
});